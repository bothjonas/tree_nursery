package tree_nursery;

public abstract class Tree {
	public Tree(int maxSize, int maxDiameter, FertilizeProcess fertilizeProcess) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.fertilizeProcess = fertilizeProcess;
	}
	private int maxSize;
	private int maxDiameter;
	private FertilizeProcess fertilizeProcess;
	public FertilizeProcess getFertilizeProcess() {
		return fertilizeProcess;
	}

}
