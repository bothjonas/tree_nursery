package tree_nursery;

import java.util.ArrayList;
import java.util.List;

public class main {

	public static void main(String[] args) {
		
		
		List<tree> trees = new ArrayList();
		GrowingArea grow1 = new GrowingArea("grow1", 29, trees);
		FertilizeProcess topg = new TopGreen();
		FertilizeProcess superg = new SuperGrow();
		tree con1 = new conifer(10, 10,topg);
		tree broad1 = new broadleaf(14,15,superg);
		
	
		trees.add(con1);
		trees.add(broad1);
		
	}

}
